#include <iostream>

//#include "spdlog/spdlog.h"

//#include "web/http.h"
//#include "web/robotscontroller.h"
#include "db/IndexQueueDAOImpl.h"
#include "web/http.h"
#include "crawler/crawler.h"

using namespace std;
using namespace web;
//using namespace spdlog;

int main(int argc, char *argv[])
{
    Crawler crawler;

    crawler.init ();
    crawler.start ();

    return 0;
}
