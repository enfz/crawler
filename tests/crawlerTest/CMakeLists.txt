include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})

add_executable(runCrawlerTest
        crawlerTest.cpp)

target_link_libraries(runCrawlerTest gmock gmock_main)
target_link_libraries(runCrawlerTest pq pqxx)
target_link_libraries(runCrawlerTest curl)
target_link_libraries(runCrawlerTest src)
