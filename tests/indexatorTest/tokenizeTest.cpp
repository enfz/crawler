//
// Created by user on 7/26/16.
//

#include <gmock/gmock.h>

#include "index/indexator.h"
#include "indexatorHeader.h"

TEST (indexator, shouldSplitIntoTokens) {
    Indexator i;

    EXPECT_THAT (i.tokenize (pageText), ::testing::ContainerEq (expectedTokens));
}
