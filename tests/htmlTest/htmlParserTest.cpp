//
// Created by user on 7/25/16.
//

#include <gmock/gmock.h>

#include "HTML/htmlParser.h"
#include "htmlParserTest.h"
#include "web/Page.h"

TEST (htmlParserTest, shouldParseText) {
    htmlParser parser;
    Page page;
    page.html = html;

    parser.parse (page);

    ASSERT_STREQ (page.text.data (), expectedText.data ());
}

TEST (htmlParserTest, shouldExtractLinks) {
    htmlParser parser;
    Page page;
    page.html = html;

    parser.parse (page);

    EXPECT_THAT (page.links, ::testing::ContainerEq (expectedLinks));
}

