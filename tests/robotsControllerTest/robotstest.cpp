#include <string>
#include <vector>
#include <memory>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "web/robotscontroller.h"
#include "web/http.h"
#include "db/robotstxtDb.h"

using namespace web;
using namespace std;
using ::testing::Return;

class MockRobotstxtDb : public RobotstxtDb {
public:
    MockRobotstxtDb () {}
    virtual ~MockRobotstxtDb () {}

    MOCK_CONST_METHOD1 (isInDb, bool(const std::string&));
    MOCK_CONST_METHOD2 (insertRules, void(vector<string>, int));
    MOCK_CONST_METHOD1 (isPageDisallowed, bool(std::string&));
    MOCK_CONST_METHOD2 (insertRobotstxt, int(std::string&, bool));
};

class MockHTTP : public HTTP {
public:
    MOCK_CONST_METHOD1 (get, std::string(std::string&));
};

string page = "https://blah-land.org/main";
string site = "https://blah-land.org";
string robotsPath = page + "/robots.txt";

TEST (robotsText, pageIsDisallowed) {
    auto robotstxtDb = make_shared<MockRobotstxtDb> ();
    auto http = make_shared<MockHTTP> ();

    EXPECT_CALL (*robotstxtDb, isInDb (site))
            .WillOnce (Return (true));

    EXPECT_CALL (*robotstxtDb, isPageDisallowed (page))
                 .WillOnce (Return (true));

    EXPECT_CALL (*http, get (robotsPath))
                 .Times (0);

    RobotsController c (http, robotstxtDb);

    ASSERT_FALSE (c.allowed (page));
}

