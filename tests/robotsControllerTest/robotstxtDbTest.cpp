//
// Created by user on 7/24/16.
//

#include <string>

#include <gtest/gtest.h>

#include <db/robotstxtDb.h>

using namespace std;

RobotstxtDb db;

TEST (robotstxtDbTest, isInDbShouldReturnTrue) {
    string site ("https://blah-land.org");

    ASSERT_TRUE (db.isInDb (site));
}

TEST (robotstxtDbTest, isInDbShouldReturnFalse) {
    string site ("https://ololo.asdf");

    ASSERT_FALSE (db.isInDb (site));
}

TEST (robotstxtDbTest, isPageDisallowedShouldReturnTrue) {
    string page ("https://blah-land.org/main");

    ASSERT_TRUE (db.isPageDisallowed (page));
}

TEST (robotstxtDbTest, isPageDisallowedShouldReturnFalse) {
    string page ("https://blah-land.org/vasya");

    ASSERT_FALSE (db.isPageDisallowed (page));
}
