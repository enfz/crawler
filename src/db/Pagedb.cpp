//
// Created by user on 8/17/16.
//

#include "Pagedb.h"

using namespace std;
using namespace pqxx;

Pagedb::Pagedb ()
        : conn (make_shared<connection> ("dbname=index user=admin"))
{}

Pagedb::Pagedb (shared_ptr<connection> newConn)
        : conn (newConn)
{}

Pagedb::~Pagedb () {}

void Pagedb::saveURL (Page& page) {
    work w (*conn);

    auto result = w.exec (
            "SELECT * FROM insertorselectid (" + w.quote (page.address) + ");"
    );
    w.commit ();

    page.id = result[0][0].as<unsigned int> ();
}
