//
// Created by user on 7/25/16.
//

#include <gumbo.h>
#include "htmlParser.h"

using namespace std;

htmlParser::htmlParser () {

}
htmlParser::~htmlParser () {

}

string htmlParser::extractText (const std::string& html) {
    auto output = gumbo_parse (html.data ());

    string text;
    this->dumpText (output->root, text);

    gumbo_destroy_output (&kGumboDefaultOptions, output);

    return text.substr (0, text.size () - 1);
}

void htmlParser::dumpText (const GumboNode* node, string& contents) {
    if (node->type == GUMBO_NODE_TEXT) {
        contents.append (node->v.text.text);
    } else if (node->type == GUMBO_NODE_ELEMENT &&
            node->v.element.tag != GUMBO_TAG_SCRIPT &&
            node->v.element.tag != GUMBO_TAG_STYLE
    ) {
        auto children = &node->v.element.children;

        for (size_t i = 0; i < children->length; ++i) {
            if (contents.back () != ' ' && contents.length () > 0)
                contents.append (" ");

            this->dumpText (static_cast<GumboNode*> (children->data[i]), contents);
        }
    }
}

vector<string> htmlParser::extractLinks (const std::string& html) {
    auto gumboOutput = gumbo_parse (html.data ());
    vector<string> links;

    this->dumpLinks (gumboOutput->root, links);

    gumbo_destroy_output (&kGumboDefaultOptions, gumboOutput);

    return links;
}

void htmlParser::dumpLinks (const GumboNode* node, std::vector<std::string>& links) {
    if (node->type == GUMBO_NODE_ELEMENT) {
        GumboAttribute* href;

        if (node->v.element.tag == GUMBO_TAG_A &&
                (href = gumbo_get_attribute (&node->v.element.attributes, "href"))
        )
            links.push_back (href->value);

        const GumboVector* children = &node->v.element.children;

        for (size_t i = 0; i < children->length; ++i) {
            dumpLinks ((GumboNode*) children->data[i], links);
        }
    }
}

void htmlParser::parse (Page& page) {
    page.text = this->extractText (page.html);
    page.links = this->extractLinks (page.html);
}
