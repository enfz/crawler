#ifndef BADURLERROR_H
#define BADURLERROR_H

#include <exception>

class BadURLError : public std::exception
{
    const char* message = nullptr;

public:
    BadURLError() noexcept {}
    BadURLError (const char* msg) noexcept
        : message (msg)
    {}

    ~BadURLError () {}

    const char* what () const noexcept override
    {
//        return this->message;
        return "URL is invalid";
    }
};

#endif // BADURLERROR_H
