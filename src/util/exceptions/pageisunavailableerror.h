#ifndef PAGEISUNAVAILABLEERROR_H
#define PAGEISUNAVAILABLEERROR_H

#include <exception>


class PageIsUnavailableError : public std::exception
{
    const char* message = nullptr;

public:
    PageIsUnavailableError() noexcept {}
    PageIsUnavailableError (const char* msg) noexcept
        : message (msg)
    {}

    ~PageIsUnavailableError () {}

    const char* what () const noexcept override
    {
        return this->message;
    }
};

#endif // PAGEISUNAVAILABLEERROR_H
