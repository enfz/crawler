//
// Created by user on 7/26/16.
//

#ifndef CRAWLER_FUNC_H_H
#define CRAWLER_FUNC_H_H

template <typename T>
concept bool VoidFunc () {
    return requires (T a) {
        {a ()} -> void;
    };
}

#endif //CRAWLER_FUNC_H_H
