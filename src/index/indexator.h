//
// Created by user on 7/26/16.
//

#ifndef CRAWLER_INDEXATOR_H
#define CRAWLER_INDEXATOR_H

#include <vector>
#include <string>

#include <web/Page.h>

class Indexator {
public:
    Indexator ();
    ~Indexator ();

    /*!
     * \brief start indexing web page
     */
    void start (Page& ) const;

    /*!
     * \brief split text into a list of tokens
     * @param text to tokenize
     * @returns a vector of tokens
     */
    std::vector<std::string> tokenize (std::string& ) const;

    /*!
     * Tokens stemming.
     * @param list of tokens
     * @returns list of stemmed tokens
     *
     * Porter`s algorithm
     */
    std::vector<std::string>& stem (std::vector<std::string>&) const;

private:

public:
};


#endif //CRAWLER_INDEXATOR_H
